using Xunit;
using GildedRose.Console;
using System.Collections.Generic;

namespace GildedRose.Tests
{
    public class ItemQualityTests
    {

        Item testItem;

        [Fact]
        public void Item_Quality_ShouldNotDecreaseMinQuality()
        {
            testItem = new Item { Name = ItemNames.Plus5DexterityVest, SellIn = 10, Quality = ItemBsn.QUALITY_MIN };

            TestSetup.PerformCalculation(testItem);

            Assert.Equal(testItem.Quality, ItemBsn.QUALITY_MIN);
        }

        [Fact]
        public void Item_Quality_ShouldNotIncreaseMaxQuality()
        {
            TestSetup.SetFeatures();

            List<string> itemNamesQulityIncreaseOverTime = (TestSetup.bsnFeature[FeatureBsn.Feature.QualityIncreasesOverTime]);

            if (itemNamesQulityIncreaseOverTime != null && itemNamesQulityIncreaseOverTime.Count > 0)
            {
                testItem = new Item { Name = itemNamesQulityIncreaseOverTime[0], SellIn = 10, Quality = ItemBsn.QUALITY_MAX };

                TestSetup.PerformCalculation(testItem);

                Assert.Equal(testItem.Quality, ItemBsn.QUALITY_MAX);
            }
        }

        //[Fact]
        //public void Item_Quality_ShouldNotInceaseMaxQuality()
        //{
        //    testItem = TestItems.GetItem_QualityEqualsMax();

        //    TestSetup.PerformCalculation(testItem);

        //    Assert.Equal(testItem.Quality, ItemBsn.QUALITY_MAX);
        //}

        //[Fact]
        //public void Item_Quality_ShouldNotChangeForSpecialItem()
        //{
        //    testItem = TestItems.GetItem_QualityShouldNotChange();

        //    if (testItem != null)
        //    {
        //        int _initQuality = testItem.Quality;

        //        TestSetup.PerformCalculation(testItem);

        //        Assert.Equal(testItem.Quality, _initQuality);
        //    }
        // }

        //[Fact]
        //public void Item_Quality_ShouldIncerasesOverTime()
        //{
        //    testItem = TestItems.GetItem_QualityIncerasesOverTime();

        //    if (testItem != null)
        //    {
        //        int _initQuality = testItem.Quality;

        //        TestSetup.PerformCalculation(testItem);

        //        Assert.True(testItem.Quality > _initQuality);
        //    }
        //}





    }
}