using Xunit;
using GildedRose.Console;
using System.Collections.Generic;

namespace GildedRose.Tests
{

    public class TestItems
    {
        public static Item GetItem_QualityEqualsMin()
        {
            return new Item() { Name = ItemNames.AgedBrie, Quality = ItemBehaviour.QUALITY_MIN, SellIn = 0 };
        }

        public static Item GetItem_QualityEqualsMax()
        {
            return new Item() { Name = ItemNames.AgedBrie, Quality = ItemBehaviour.QUALITY_MAX, SellIn = 0 };
        }

        //public static Item GetItem_QualityShouldNotChange()
        //{
        //    List<string> itemNames = ItemNames.DoesNotChangeQuality();

        //    if(itemNames.Count != 0)
        //    {
        //        return new Item() { Name = itemNames[0], Quality = ItemBehaviour.QUALITY_SPECIAL_ITEM, SellIn = 0 };
        //    }

        //    return null;
        //}

        //public static Item GetItem_QualityIncerasesOverTime()
        //{
        //    List<string> itemNames = ItemNames.QualityIncreasesOverTime();

        //    if (itemNames.Count != 0)
        //    {
        //        return new Item() { Name = itemNames[0], Quality = ItemBehaviour.QUALITY_MIN, SellIn = 0 };
        //    }

        //    return null;
        //}


    }
}