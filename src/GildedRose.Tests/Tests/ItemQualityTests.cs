using Xunit;
using GildedRose.Console;
using System.Collections.Generic;
using System.Linq;
using static GildedRose.Console.FeatureBsn;

[assembly: CollectionBehavior(CollectionBehavior.CollectionPerClass, DisableTestParallelization = true)]

namespace GildedRose.Tests
{
    public class ItemQualityTests
    {

        [Fact]
        public void Item_Quality_ShouldNotDecrease_MinQuality()
        {
            Item testItem;

            testItem = new Item { Name = ItemNames.Plus5DexterityVest, SellIn = 10, Quality = ItemBsn.QUALITY_MIN };

            TestSetup.PerformCalculation(testItem);

            Assert.Equal(testItem.Quality, ItemBsn.QUALITY_MIN);
        }

        [Fact]
        public void Item_Quality_ShouldNotIncrease_MaxQuality()
        {
            TestSetup.SetFeatures();
            Item testItem;

            List<string> names = (TestSetup.bsnFeature[Feature.QualityIncreasesOverTime]);

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = 10, Quality = ItemBsn.QUALITY_MAX };

                TestSetup.PerformCalculation(testItem);

                Assert.Equal(testItem.Quality, ItemBsn.QUALITY_MAX);
            }
        }

        [Fact]

        public void Item_Quality_ShouldNotChange_ForLegendaryItem()
        {
            TestSetup.SetFeatures();
            Item testItem;

            List<string> names = (TestSetup.bsnFeature[Feature.QualityLegendary]);

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = 10, Quality = ItemBsn.QUALITY_LEGENDARY };

                TestSetup.PerformCalculation(testItem);

                Assert.Equal(testItem.Quality, ItemBsn.QUALITY_LEGENDARY);
            }
        }

        [Fact]
        public void Item_Quality_ShouldNotChange_ForLegendaryItemWithIncorrectQuality()
        {
            TestSetup.SetFeatures();
            Item testItem;

            List<string> names = (TestSetup.bsnFeature[Feature.QualityLegendary]);

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = 10, Quality = ItemBsn.QUALITY_MAX };

                TestSetup.PerformCalculation(testItem);

                Assert.Equal(testItem.Quality, ItemBsn.QUALITY_MAX);
            }
        }

        [Fact]
        public void Item_Quality_ShouldIncrease_ForQualityIncreasingItems()
        {
            TestSetup.SetFeatures();
            Item testItem;
            int _initQuality = ItemBsn.QUALITY_MIN;

            List<string> names = (TestSetup.bsnFeature[Feature.QualityIncreasesOverTime]);

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = 10, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality > _initQuality);
            }
        }

        [Fact]
        public void Item_Quality_ShouldDecrease_ForQualityDecreasingItems()
        {
            TestSetup.SetFeatures();
            Item testItem;
            int _initQuality = ItemBsn.QUALITY_MAX;

            List<string> namesQualityIncrease = (TestSetup.bsnFeature[Feature.QualityIncreasesOverTime]);

            List<string> allPossibleNames = typeof(ItemNames).GetAllPublicConstantValues<string>();

            List<string> names = allPossibleNames.Except(namesQualityIncrease).ToList();

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = 10, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality < _initQuality);
            }
        }

        [Fact]
        public void Item_Quality_ShouldDecreaseTwiceFaster_ForConjuredItem()
        {
            Item testItem;
            TestSetup.SetFeatures();

            List<string> names = (TestSetup.bsnFeature[Feature.Conjured]);
            int _initQuality = ItemBsn.QUALITY_MAX;

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = 10, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality == (_initQuality - 2 * ItemBsn.CHANGE_STEP));
            }
        }

        [Fact]
        public void Item_Quality_ShouldIncrease_ForCustomChangeModelItem()
        {
            Item testItem;
            TestSetup.SetFeatures();

            List<string> names = (TestSetup.bsnFeature[Feature.QualityCustomChangeModel]);
            int _initQuality = ItemBsn.QUALITY_MIN;
            int _initSelin = ItemBsn.CUSTOM_SELLIN_UPPER_BOUND + 1;

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = _initSelin, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality == (_initQuality + ItemBsn.CHANGE_STEP));
            }
        }

        [Fact]
        public void Item_Quality_ShouldIncreaseTwiceFaster_ForCustomChangeModelItem()
        {
            Item testItem;
            TestSetup.SetFeatures();
            int _initSelin = ItemBsn.CUSTOM_SELLIN_UPPER_BOUND - 1;

            List<string> names = (TestSetup.bsnFeature[Feature.QualityCustomChangeModel]);
            int _initQuality = ItemBsn.QUALITY_MIN;

            if (names != null && names.Count > 0 && _initSelin > ItemBsn.CUSTOM_SELLIN_LOWER_BOUND)
            {
                testItem = new Item { Name = names[0], SellIn = _initSelin, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality == (_initQuality + 2 * ItemBsn.CHANGE_STEP));
            }
        }


        [Fact]
        public void Item_Quality_ShouldIncreaseThreeTimesFaster_ForCustomChangeModelItem()
        {
            Item testItem;
            TestSetup.SetFeatures();

            List<string> names = (TestSetup.bsnFeature[Feature.QualityCustomChangeModel]);
            int _initQuality = ItemBsn.QUALITY_MIN;
            int _initSelin = ItemBsn.CUSTOM_SELLIN_LOWER_BOUND - 1;

            if (names != null && names.Count > 0 && _initSelin > 0)
            {
                testItem = new Item { Name = names[0], SellIn = _initSelin, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality == (_initQuality + 3 * ItemBsn.CHANGE_STEP));
            }
        }

        [Fact]
        public void Item_Quality_ShouldDropToZeroAfterSellinDate_ForCustomChangeModelItem()
        {
            Item testItem;
            TestSetup.SetFeatures();

            List<string> names = (TestSetup.bsnFeature[Feature.QualityCustomChangeModel]);
            int _initQuality = ItemBsn.QUALITY_MIN;
            int _initSelin = 0;

            if (names != null && names.Count > 0 && _initSelin > 0)
            {
                testItem = new Item { Name = names[0], SellIn = _initSelin, Quality = _initQuality };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.Quality == 0);
            }
        }

    }
}