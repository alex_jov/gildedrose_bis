using Xunit;
using GildedRose.Console;
using System.Collections.Generic;
using System.Linq;

namespace GildedRose.Tests
{
    public class ItemSellinTests
    {

        [Fact]
        public void Item_Selin_ShouldNotChange_ForLegendaryItem()
        {
            Item testItem;
            TestSetup.SetFeatures();

            List<string> names = (TestSetup.bsnFeature[FeatureBsn.Feature.QualityLegendary]);
            int _initSelin = 0;

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = _initSelin, Quality = ItemBsn.QUALITY_LEGENDARY };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.SellIn == _initSelin);
            }
        }

        [Fact]
        public void Item_Sellin_ShouldDecrease_ForNonLegendaryItems()
        {
            Item testItem;
            TestSetup.SetFeatures();
            int _initSelin = 10;

            List<string> namesLegendary = (TestSetup.bsnFeature[FeatureBsn.Feature.QualityLegendary]);

            List<string> allPossibleNames = typeof(ItemNames).GetAllPublicConstantValues<string>();

            List<string> names = allPossibleNames.Except(namesLegendary).ToList();

            if (names != null && names.Count > 0)
            {
                testItem = new Item { Name = names[0], SellIn = _initSelin, Quality = ItemBsn.QUALITY_MAX };

                TestSetup.PerformCalculation(testItem);

                Assert.True(testItem.SellIn < _initSelin);
            }
        }




    }
}