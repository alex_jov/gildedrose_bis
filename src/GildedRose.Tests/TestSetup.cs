using Xunit;
using GildedRose.Console;
using System.Collections.Generic;
using static GildedRose.Console.FeatureBsn;

namespace GildedRose.Tests
{
    public class TestSetup
    {
        public static FeatureBsn bsnFeature = new FeatureBsn();
        private static IList<Item> Items;

        public static void PerformCalculation(Item item)
        {
            Items = new List<Item>();
            Items.Add(item);

            Item testItem = new Item { Name = ItemNames.AgedBrie, SellIn = 10, Quality = 40 };
            Items.Add(testItem);

            if (bsnFeature == null)
            {
                SetFeatures();
            }

            // new
            ItemBsn.DailyItemAdjustment(Items);

            // previous
            //Program program = new Console.Program();
            //Program.Items = Items;
            //program.UpdateQuality();
        }

        public static void SetFeatures()
        {
            bsnFeature[Feature.QualityImmutable] = new List<string>() { ItemNames.SulfurasHandOfRagnaros };
            bsnFeature[Feature.QualityLegendary] = new List<string>() { ItemNames.SulfurasHandOfRagnaros };
            bsnFeature[Feature.QualityIncreasesOverTime] = new List<string>() { ItemNames.AgedBrie, ItemNames.BackstagePasses };
            bsnFeature[Feature.QualityCustomChangeModel] = new List<string>() { ItemNames.BackstagePasses };
            bsnFeature[Feature.Conjured] = new List<string>() { ItemNames.ConjuredManaCake };
           
        }

        }
}