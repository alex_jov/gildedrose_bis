﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console
{

    

    /// <summary>
    /// Holds Item.Name strings
    /// To add new item name, add constant
    /// </summary>
    public static class ItemNames // dictionary - map to items (list)
    {
        public static readonly string Plus5DexterityVest = "+5 Dexterity Vest";
        public static readonly string AgedBrie = "Aged Brie";
        public static readonly string ElixirOfTheMongoose = "Elixir of the Mongoose";
        public static readonly string SulfurasHandOfRagnaros = "Sulfuras, Hand of Ragnaros";
        public static readonly string BackstagePasses = "Backstage passes to a TAFKAL80ETC concert";
        public static readonly string ConjuredManaCake = "Conjured Mana Cake";

        // todo remove
        // VERSION 1
        public static List<string> GetDoesNotChangeQuality()
        {
            return new List<string>() { ItemNames.SulfurasHandOfRagnaros };
        }

        public static List<string> GetHasLegendaryQualityValue()
        {
            return new List<string>() { ItemNames.SulfurasHandOfRagnaros };
        }

        public static List<string> GetQualityIncreasesOverTime()
        {
            return new List<string>() { ItemNames.AgedBrie, ItemNames.BackstagePasses };
        }

        public static List<string> GetHaveCustomQualityChangeModel()
        {
            return new List<string>() { ItemNames.BackstagePasses };
        }

        public static List<string> GetConjured()
        {
            return new List<string>() { ItemNames.ConjuredManaCake };
        }


        //public static Dictionary<string, List<Item>> itemNames = new Dictionary<string, List<Item>>();

        //public enum CustomProperty
        //{
        //    DoesNotChangeQuality = 1,
        //    HasLegendaryQualityValue = 2,
        //    QualityIncreasesOverTime = 3,
        //    HaveCustomQualityChangeModel = 4
        //}

        //public static Dictionary<CustomProperty, List<string>> itemNamesCustomProperties = new Dictionary<CustomProperty, List<string>>();


        //public static void AddItem(Item item)
        //{
        //    itemNames[item.Name].Add(item);
        //}

        //// todo - how often item properties change? set them
        //public static void SetItemNameCustomProperty(List<Item> items, CustomProperty customProperty)
        //{
        //    foreach (var item in items)
        //    {
        //        itemNamesCustomProperties[customProperty].Add(item.Name);
        //    }
        //}

        //public static void RemoveItemNameCustomProperty(Item item, CustomProperty customProperty)
        //{
        //    itemNamesCustomProperties[customProperty].Remove(item.Name);
        //}

        //public static List<string> GetItemNameCustomProperty(CustomProperty customProperty)
        //{
        //    return itemNamesCustomProperties[customProperty];
        //}


        //private static List<Item> result;
        //private static List<string> names;



        //public static List<Item> DoesNotChangeQuality {
        //    get {
        //        result = new List<Item>();
        //        names = GetItemNameCustomProperty(CustomProperty.DoesNotChangeQuality);
        //        foreach (var name in names)
        //        {
        //            result.AddRange(itemNames[name]);
        //        }
        //        return result;
        //    }
        //    set
        //    {
        //        SetItemNameCustomProperty(value, CustomProperty.DoesNotChangeQuality);
        //    }
        //}


        //public static List<Item> HasLegendaryQualityValue
        //{
        //    get {
        //        result = new List<Item>();
        //        result.AddRange(itemNames["Sulfuras, Hand of Ragnaros"]);
        //        return result;
        //    }
        //}

        //public static List<Item> QualityIncreasesOverTime
        //{
        //    get {
        //        result = new List<Item>();
        //        result.AddRange(itemNames["Aged Brie"]);
        //        result.AddRange(itemNames["Backstage passes to a TAFKAL80ETC concert"]);
        //        return result;
        //    }
        //}

        //public static List<Item> HaveCustomQualityChangeModel
        //{
        //    get {
        //        return itemNames["Aged Brie"];
        //    }
        //}

        //public static List<Item> Conjured
        //{
        //    get {
        //        return itemNames["Sulfuras, Hand of Ragnaros"];
        //    }
        //}

        //



    }
}
