﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console
{

    /// <summary>
    /// Holds Item.Name strings
    /// To add new item name, add constant
    /// </summary>
    public static class ItemNames 
    {
        public const string Plus5DexterityVest = "+5 Dexterity Vest";
        public const string AgedBrie = "Aged Brie";
        public const string ElixirOfTheMongoose = "Elixir of the Mongoose";
        public const string SulfurasHandOfRagnaros = "Sulfuras, Hand of Ragnaros";
        public const string BackstagePasses = "Backstage passes to a TAFKAL80ETC concert";
        public const string ConjuredManaCake = "Conjured Mana Cake";

    }

   
}
