﻿using System.Collections.Generic;
using static GildedRose.Console.FeatureBsn;

namespace GildedRose.Console
{
    public class Program // todo do not leave public
    {
        public static IList<Item> Items;
        public static string initialLine = "OMGHAI!";

        static void Main(string[] args)
        {
            System.Console.WriteLine(initialLine);

            Items = new List<Item>();

            // add items
            Items.Add(new Item { Name = ItemNames.Plus5DexterityVest, SellIn = 10, Quality = 20 } );
            Items.Add(new Item { Name = ItemNames.AgedBrie, SellIn = 2, Quality = 0 } );
            Items.Add(new Item { Name = ItemNames.ElixirOfTheMongoose, SellIn = 5, Quality = 7 } );
            Items.Add(new Item { Name = ItemNames.SulfurasHandOfRagnaros, SellIn = 0, Quality = 80 } );
            Items.Add(new Item { Name = ItemNames.BackstagePasses, SellIn = 15, Quality = 20 } );
            Items.Add(new Item { Name = ItemNames.ConjuredManaCake, SellIn = 3, Quality = 6 } );

            // add properties
            FeatureBsn bsnFeature = new FeatureBsn();
            bsnFeature[Feature.QualityImmutable] = new List<string>() { ItemNames.SulfurasHandOfRagnaros };
            bsnFeature[Feature.QualityLegendary] = new List<string>() { ItemNames.SulfurasHandOfRagnaros };
            bsnFeature[Feature.QualityIncreasesOverTime] = new List<string>() { ItemNames.AgedBrie, ItemNames.BackstagePasses };
            bsnFeature[Feature.QualityCustomChangeModel] = new List<string>() { ItemNames.BackstagePasses };
            bsnFeature[Feature.Conjured] = new List<string>() { ItemNames.ConjuredManaCake };

            // perform adjustment
            ItemBsn.DailyItemAdjustment(Items);

            //var app = new Program()
            //              {
            //                  Items = new List<Item>
            //                              {
            //                                  new Item {Name = ItemNames.Plus5DexterityVest, SellIn = 10, Quality = 20},
            //                                  new Item {Name = ItemNames.AgedBrie, SellIn = 2, Quality = 0},
            //                                  new Item {Name = ItemNames.ElixirOfTheMongoose, SellIn = 5, Quality = 7},
            //                                  new Item {Name = ItemNames.SulfurasHandOfRagnaros, SellIn = 0, Quality = 80},
            //                                  new Item {Name = ItemNames.BackstagePasses, SellIn = 15, Quality = 20},
            //                                  new Item {Name = ItemNames.ConjuredManaCake, SellIn = 3, Quality = 6}
            //                              }

            //              };

            //app.UpdateQuality();

            System.Console.ReadKey();

        }

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                if (Items[i].Name != "Aged Brie" && Items[i].Name != "Backstage passes to a TAFKAL80ETC concert")
                {
                    if (Items[i].Quality > 0)
                    {
                        if (Items[i].Name != "Sulfuras, Hand of Ragnaros")
                        {
                            Items[i].Quality = Items[i].Quality - 1;
                        }
                    }
                }
                else
                {
                    if (Items[i].Quality < 50)
                    {
                        Items[i].Quality = Items[i].Quality + 1;

                        if (Items[i].Name == "Backstage passes to a TAFKAL80ETC concert")
                        {
                            if (Items[i].SellIn < 11)
                            {
                                if (Items[i].Quality < 50)
                                {
                                    Items[i].Quality = Items[i].Quality + 1;
                                }
                            }

                            if (Items[i].SellIn < 6)
                            {
                                if (Items[i].Quality < 50)
                                {
                                    Items[i].Quality = Items[i].Quality + 1;
                                }
                            }
                        }
                    }
                }

                if (Items[i].Name != "Sulfuras, Hand of Ragnaros")
                {
                    Items[i].SellIn = Items[i].SellIn - 1;
                }

                if (Items[i].SellIn < 0)
                {
                    if (Items[i].Name != "Aged Brie")
                    {
                        if (Items[i].Name != "Backstage passes to a TAFKAL80ETC concert")
                        {
                            if (Items[i].Quality > 0)
                            {
                                if (Items[i].Name != "Sulfuras, Hand of Ragnaros")
                                {
                                    Items[i].Quality = Items[i].Quality - 1;
                                }
                            }
                        }
                        else
                        {
                            Items[i].Quality = Items[i].Quality - Items[i].Quality;
                        }
                    }
                    else
                    {
                        if (Items[i].Quality < 50)
                        {
                            Items[i].Quality = Items[i].Quality + 1;
                        }
                    }
                }
            }
        }

    }

    public class Item
    {
        public string Name { get; set; }

        public int SellIn { get; set; }

        public int Quality { get; set; }
    }

}
