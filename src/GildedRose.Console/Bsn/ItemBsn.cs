﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GildedRose.Console.FeatureBsn;

namespace GildedRose.Console
{

    public static class ItemBsn
    {
        public const int QUALITY_MIN = 0;
        public const int QUALITY_MAX = 50;
        public const int QUALITY_LEGENDARY = 80;

        public const int CHANGE_STEP = 1;
                                          
        public const int CUSTOM_SELLIN_LOWER_BOUND = 5;
        public const int CUSTOM_SELLIN_UPPER_BOUND = 10;

        public static FeatureBsn bsnFeature =  new FeatureBsn();

        private static int GetQualityChangeCoefficient(Item item)
        {
            int changeCoefficient = 1;

            if ((bsnFeature[Feature.QualityCustomChangeModel]).Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
            {
                if (item.SellIn < 0)
                {
                    changeCoefficient = 0;
                }
                else if(item.SellIn <= CUSTOM_SELLIN_UPPER_BOUND && item.SellIn > CUSTOM_SELLIN_LOWER_BOUND)
                {
                    changeCoefficient =  2;
                }
                else if (item.SellIn <= CUSTOM_SELLIN_LOWER_BOUND && item.SellIn >= 0)
                {
                    changeCoefficient =  3;
                }
            }
            else if (item.SellIn < 0 || (bsnFeature[Feature.Conjured]).Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
            {
                changeCoefficient =  2;
            }

            return changeCoefficient;
        }

        public static void DailyItemAdjustment(IList<Item> items)
        {
            int newQualityValue;

            foreach (var item in items)
            {
                // check if legendary
                if ((bsnFeature[Feature.QualityLegendary]).Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
                {
                    // do not change to be consistent with original program
                    /* if (item.Quality != QUALITY_LEGENDARY || item.SellIn != 0)
                    {
                        item.Quality = QUALITY_LEGENDARY;
                        item.SellIn = 0;
                    } */

                    continue;
                }

                // Adjust Quality
                if (!(bsnFeature[Feature.QualityImmutable]).Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
                {
                    // check if should increase or decrease
                    if (!(bsnFeature[Feature.QualityIncreasesOverTime]).Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
                    {
                        newQualityValue = item.Quality - (GetQualityChangeCoefficient(item) * CHANGE_STEP);
                    }
                    else
                    {
                        newQualityValue = item.Quality + (GetQualityChangeCoefficient(item) * CHANGE_STEP);
                    }

                    // check if update allowed
                    if (newQualityValue >= QUALITY_MIN && newQualityValue <= QUALITY_MAX)
                    {
                        item.Quality = newQualityValue;
                    }
                }

                // Adjust Sellin
                item.SellIn -= 1;
            }
        }

    }
}
