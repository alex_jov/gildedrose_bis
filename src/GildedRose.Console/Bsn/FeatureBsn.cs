﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console
{
    public class FeatureBsn
    {
        public static Dictionary<Feature, List<string>> namesByFeature = new Dictionary<Feature, List<string>>();

        public enum Feature
        {
            QualityImmutable,
            QualityLegendary,
            QualityIncreasesOverTime,
            QualityCustomChangeModel,
            Conjured
        }

        /// <summary>
        /// Gets / Sets namesByFeature
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        public List<string> this[Feature feature]
        {
            get
            {
                if (namesByFeature.ContainsKey(feature))
                {
                    return namesByFeature[feature];
                }
                else {
                    return new List<string>();
                }
            }
            set
            {
                if (namesByFeature.ContainsKey(feature))
                {
                    namesByFeature[feature].AddRange(value);
                }
                else {
                    namesByFeature.Add(feature, value);
                }
            }
        }

        /// <summary>
        /// Removes item name feature
        /// </summary>
        /// <param name="item"></param>
        /// <param name="customProperty"></param>
        public static void RemoveItemNameCustomProperty(Item item, Feature customProperty)
        {
            if (namesByFeature.ContainsKey(customProperty))
            {
                namesByFeature[customProperty].Remove(item.Name);
            }
        }

    }

}
