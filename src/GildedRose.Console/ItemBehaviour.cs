﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GildedRose.Console
{

    public static class ItemBehaviour // todo remove - obsolete
    {
        public const int QUALITY_MIN = 0;
        public const int QUALITY_MAX = 50;
        public const int QUALITY_LEGENDARY = 80;

        public const int CHANGE_STEP = 1; // decrease / increase by one
        //public const int INIT_DEGRADE_RATE = 1;

        private static int GetQualityChangeCoefficient(Item item)
        {
            List<string> itemNamesHaveCustomQualityChangeModel = ItemNames.GetHaveCustomQualityChangeModel();
            List<string> itemNamesConjured = ItemNames.GetConjured();

            int changeCoefficient = 1;

            // check if has custom quality change model
            // todo refactor
            if (itemNamesHaveCustomQualityChangeModel.Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
            {
                if (item.SellIn < 0)
                {
                    changeCoefficient = 0;
                }
                else if(item.SellIn >= 10 && item.SellIn < 5)
                {
                    changeCoefficient = 2;
                }
                else if (item.SellIn >= 5 && item.SellIn <= 0)
                {
                    changeCoefficient = 3;
                }
            }
            else if (item.SellIn < 0 || itemNamesConjured.Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
            {
                changeCoefficient = 2;
            }

            return changeCoefficient;
        }

        public static void AdjustQuality(List<Item> items)
        {
            int newQualityValue;

            // todo do not double
            List<string> itemNamesDoesNotChangeQuality = ItemNames.GetDoesNotChangeQuality();
            List<string> itemNamesHasLegendaryQualityValue = ItemNames.GetHasLegendaryQualityValue();
            List<string> itemNamesQualityIncreasesOverTime = ItemNames.GetQualityIncreasesOverTime();

            // O(N)
            foreach (var item in items) // O(N)
            {
                if (!itemNamesDoesNotChangeQuality.Contains(item.Name, StringComparer.InvariantCultureIgnoreCase)) // O(1) if hash only!
                {
                    // check if legendary
                    if (itemNamesHasLegendaryQualityValue.Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
                    {
                        // Move this code that sets values into the ItemDictionary class (was ItemNames) in the addItemTo() method
                        if (item.Quality != QUALITY_LEGENDARY)
                        {
                            item.Quality = QUALITY_LEGENDARY;
                            //System.Console.WriteLine(item.Name + " :Item quality updated - legendary item must have legendary value");
                            return; // done
                        }
                        else
                        {
                            //System.Console.WriteLine(item.Name + " :Item quality not updated - legendary item has legendary value");
                        }

                    }

                    // check if should increase or decrease
                    if (!itemNamesQualityIncreasesOverTime.Contains(item.Name, StringComparer.InvariantCultureIgnoreCase))
                    {
                        newQualityValue = item.Quality - (GetQualityChangeCoefficient(item) * CHANGE_STEP);
                    }
                    else
                    {
                        newQualityValue = item.Quality + (GetQualityChangeCoefficient(item) * CHANGE_STEP);
                    }

                   // check if update allowed
                    if (newQualityValue <= QUALITY_MIN || newQualityValue >= QUALITY_MAX)
                    {
                        item.Quality = newQualityValue;
                        return; // done
                    }
                    else
                    {
                        //System.Console.WriteLine(item.Name + " :Item quality not updated - exceeds allowed values");
                    }
                } else
                {
                    //System.Console.WriteLine(item.Name + " :Item quality not updated - does not change quality over time");
                }
            }
        }

        public static void AdjustSellIn(List<Item> items)
        {
            foreach (var item in items)
            {
                item.SellIn -= 1;
            }


        }

    }
}
